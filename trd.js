/*
MIT License

Copyright (c) 2023 Radim Kolar

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice (including the next paragraph) shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

import WebTorrent from 'webtorrent';
import { encode as magnetEncode, decode as magnetDecode } from 'magnet-uri';

import fs from 'node:fs';

const client = new WebTorrent({dht: { concurrency: 32 }, lsd: true, blocklist: null, tracker: false});
const arg = process.argv[2];
let magnetURI;
if (arg.startsWith("magnet:")) {
   const decoded = magnetDecode(arg)
   const hash = decoded.infoHash
   // console.log(`Decoded hash from magnet ${hash}`)
   magnetURI = magnetEncode( { infoHash: hash } )
} else {
   magnetURI = magnetEncode({ infoHash: arg })
}
console.log(magnetURI)

const t = client.add(magnetURI, { private: false } );

t.on('infoHash', function() {
    console.log("infohash known")
    const torrentFilePath = `${this.infoHash}.torrent`
    t.on('metadata', function() {
       console.log("got metadata event")
       fs.writeFileSync(torrentFilePath, this.torrentFile)
       console.log(`Saved .torrent file to ${torrentFilePath}`);
 
       client.destroy(err => {
         if (err) {
            return err
         }
       })
  });

  });

t.on('error', function (err) {
  console.error('Fatal Error: ' + err.message)
  client.destroy()
});
